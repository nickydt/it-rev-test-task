const initialState = {
  data: []
};

const records = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_DATA':
        return {
          data: action.data
        }
    default:
      return state;
  }
}

export default records