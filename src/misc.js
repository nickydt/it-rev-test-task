function addSuffix(dist) {
    dist = dist%100;
    if (dist===0 || (dist>4 && dist<20) || (dist%10>4) || (dist%10===0)) {
        return "ов"
    }
    if (dist===1) {
        return ""
    }
    return "a"
}

export function formatDistance(distance) {
    distance = parseInt(distance);
    
    if (!distance) {
        return "0 метров"
    }
    
    let res = [];
    let meters = distance%1000;
    let kilometers = (distance-meters)/1000;
    
    if (kilometers) {
        res.push(kilometers, "километр"+addSuffix(kilometers));
    }
    if (meters) {
        res.push(meters, "метр"+addSuffix(meters));
    }
    
    return res.join(" ")
}

export function formatDate(date) {
    return [date.slice(8,10), date.slice(5,7), date.slice(0,4)].join(".");
}

export function getWeekday(date) {
    let day = new Date(date);
    // eslint-disable-next-line default-case
    switch (day.getDay()) {
        case 1:
            return "Понедельник"
        case 2:
            return "Вторник"
        case 3:
            return "Среда"
        case 4:
            return "Четверг"
        case 5:
            return "Пятница"
        case 6:
            return "Суббота"
        case 7:
            return "Воскресенье"
      }
}