import React from 'react';
import '../app.css';
import { formatDistance, formatDate, getWeekday } from '../misc.js'

export default function DataList(props) {
    return (
        <div className="record">
            <div>
                <span>{ getWeekday(props.date) }</span><br />
                { formatDate(props.date) }
            </div>
            <div>
                { formatDistance(props.distance) }
            </div>
        </div>
    );
}