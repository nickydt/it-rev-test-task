import React from 'react';
import './app.css';
import Header from './components/header';
import DataList from './containers/datalist';

function App() {
  return [
    <Header key="header"/>,
    <main key="main">
      <div className="main_block">
        <DataList />
      </div>
    </main>
  ];
}

export default App;
