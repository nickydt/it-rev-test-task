import React from 'react';
import '../app.css';
import Record from '../components/record';
import { connect } from 'react-redux';

export function DataList(props) {
    return (
        props.data.map((record) => 
        <Record date={record.date} distance={record.distance} key={record.id}/>,
        )
    );
}

const mapStateToProps = function(store) {
  return {
    data: store.data
  };
}

export default connect(mapStateToProps)(DataList)